import './App.css';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faBars, faCheckSquare, faCoffee,faFire,faPhone, faPlayCircle, faSearch, faTimes} from '@fortawesome/free-solid-svg-icons'
import Navbar from './components/Navbar';
import {BrowserRouter as Router, Switch,Route} from 'react-router-dom'
import Home from './components/pages/Home';
import Services from './components/pages/Services';
import Products from './components/pages/Products';
import SignUp from './components/pages/SignUp';

library.add(faCheckSquare, faCoffee,faPhone,faTimes,faBars,faSearch,faFire,faPlayCircle)

function App() {
  return (
    <Router>
      <Navbar/>
      <Switch>
      <Route path="/" exact component={Home} />
        <Route path="/services" component={Services}/>
        <Route path="/products" component={Products}/>
        <Route path="/sign-up" component={SignUp}/>
      </Switch>
      </Router>
  
  );
}

export default App;
