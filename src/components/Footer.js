import React from 'react'
import { Link } from 'react-router-dom'
import {Button} from './Button'
import './Footer.css'

function Footer() {
    return (
        <div className="footer-container">
            <section className="footer-subscription">

                <p className="footer-subscription-heading">
                    Join the Adventure newsletter to receive our best Vacation Deals
                </p>

                <p className="footer-subscription-text">
                    You can unsubscribe at any time
                </p>

                <div className="input-areas">
                    <form>

                        <input
                         type="text"
                         name="email"
                         placeholder="Your Email"
                         className="footer-input"
                        
                        />

                        <Button
                        buttonStyle='btn-outline'
                        >
Subscribe
                        </Button>

                        
                    </form>
                </div>
            </section>

            <div className="footer-links">
                <div className="footer-link-wrapper">
                    <div className="foooter-link-items">
                        <h2>About Us</h2>
                        <Link to='/sign-up'>How it works</Link>
                        <Link to='/'>Testimonials</Link>
                        <Link to='/'>Careers</Link>
                        <Link to='/'>Investors</Link>
                        <Link to='/'>Terms of Service</Link>
                    </div>
                    <div className="foooter-link-items">
                        <h2>Countact Us</h2>
                        <Link to='/sign-up'>How it works</Link>
                        <Link to='/'>Testimonials</Link>
                        <Link to='/'>Careers</Link>
                        <Link to='/'>Investors</Link>
                        <Link to='/'>Terms of Service</Link>
                    </div>
                    
                </div>
                <div className="footer-link-wrapper">
                    <div className="foooter-link-items">
                        <h2>Social Media</h2>
                        <Link to='/sign-up'>How it works</Link>
                        <Link to='/'>Testimonials</Link>
                        <Link to='/'>Careers</Link>
                        <Link to='/'>Investors</Link>
                        <Link to='/'>Terms of Service</Link>
                    </div>
                    <div className="foooter-link-items">
                        <h2>Video</h2>
                        <Link to='/sign-up'>How it works</Link>
                        <Link to='/'>Testimonials</Link>
                        <Link to='/'>Careers</Link>
                        <Link to='/'>Investors</Link>
                        <Link to='/'>Terms of Service</Link>
                    </div>
                    
                </div>
            </div>
            
            <section className="social-media">
                <div className="social-media-wrap">
                    <div className="footer-logo">
                        i love logo
                    </div>
                </div>
            </section>
        </div>
    )
}

export default Footer
