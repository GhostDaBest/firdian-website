import React,{useState,useEffect} from 'react'
import {Link} from 'react-router-dom'
import { faBars} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Button } from './Button'
import './Navbar.css'


export default function Navbar() {

    const [click,setClick]  = useState(false)

    const [button,setButton] = useState(true)

    const [search, setSearch]=useState(false)

    const closeMobileMenu = () => setClick(false)
    const showButton = () => {
        if(window.innerWidth <= 960){
            setButton(false)
        }
        else{
            setButton(true)
            setSearch(false)
            setClick(false)
        }
    }

    useEffect(() => {
        showButton();
    }, [])

    window.addEventListener('resize',showButton)
  


    return (
        <>
        <nav className="navbar">
        <div className="navbar-container">
           {
               !search &&  <Link to="/" className="navbar-logo" onClick={closeMobileMenu} >
               FDT<FontAwesomeIcon icon="fire" />
           </Link>
           }

           <div className="form">
           {
                search&& <input type="text"  className="form-control" />
            }
           </div>
            <div className="menu-icon" >
               <div onClick={()=>setClick(prevClick=>!prevClick)}>
               {(click && !search) && <FontAwesomeIcon icon="times" /> }
               { (!click && !search) && <FontAwesomeIcon icon="bars" />}
               </div>

               <div onClick={()=>setSearch(prevClick=>!prevClick)}>
               {search ? <FontAwesomeIcon icon="times" /> : <FontAwesomeIcon icon="search" />}

               </div>
            </div>

           

            <ul className={(click && !search)?'nav-menu active':'nav-menu'}>

                <Link to="/" className='nav-links' onClick={closeMobileMenu}>
                    Home
                </Link>

                <Link to="services" className='nav-links' onClick={closeMobileMenu}>
                    Services
                </Link>
                <Link to="products" className='nav-links' onClick={closeMobileMenu}>
                    Products
                </Link>
                <Link to="sign-up" className='nav-links-mobile' onClick={closeMobileMenu}>
                    Sign Up
                </Link>
            </ul>
             {(button&& !search) && <Button buttonStyle="btn-outline">SIGN UP</Button>}
        </div>
        </nav>
            
        </>
    )
}
